package eu.kstaykov.snowdrop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration
public class SnowdropApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnowdropApplication.class, args);
	}
}
